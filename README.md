*  https://ewastesf.com/
*  Frontend: HTML, CSS, JavaScript, Vue.JS, Vuex, Webpack, Babel, jQuery
*  Backend: Django, Python, AWS (S3, SES)
*  Deployment: Nginx, Gunicorn, EC2

*  This directory showcases a portion of my work as a Full-stack Developer at ewasteSF. These images include internal CMS / CRM interfaces (with test data) and other administrative tools.

*  For a user dashboard demo, please click [here](https://ewastesf.com/portal/?id=democompany&code=portal).

*  At my previous company ewasteSF, I initiated and led the transition of using third-party software into developing completely in-house web applications in order to satisfy the need of the company’s expanding services.
*  At ewasteSF, I
    * Designed and developed scalable and responsive e-commerce, CMS, and CRM applications from the ground up using Vue.js and Django.
    * Performed recycling data analytic and tracking for customer dashboards serving 200+ office buildings including and several Big N companies in San Francisco.
    * Integrated Webpack with Babel to ensure cross browser compatibility.
    * Designed and implemented databases in SQLite and performed queriesfor Django back-end.
    * Deployed Django applications using Gunicorn and Nginx in AWS EC2.
    * Integrated a number of APIs such as Stripe and PayPal payment gateways, OAuth Sign-In, and AWS applications.
    * Leveraged Google Analytics to create custom metric reports and enhance SEO, thereby reducing bounce rate by 20% and increasing conversion rate by 40%.

<img src="/screenshots/dashboard1.png" alt="dashboard1" width="400"/>
<img src="/screenshots/dashboard2.png" alt="dashboard2" width="400"/>
<img src="/screenshots/dashboard3.png" alt="dashboard3" width="400"/>
<img src="/screenshots/portal1.png" alt="portal1" width="400"/>
<img src="/screenshots/portal2.png" alt="portal2" width="400"/>
<img src="/screenshots/portal3.png" alt="portal3" width="400"/>
